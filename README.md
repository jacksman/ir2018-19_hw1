# IR2018-19 HW1
Autore: Giacomo Sartori  
Matricola: 1183544  

---

## Contenuto Repository

#### FILEs
* __IR2018-19\_HW1.ipynb :__ jupyter notebook utilizzato per sviluppare i test ANOVA One-way e di Tukey
* __tester.py :__ script python che permette l'esecuzione automatica dei test. Per l'utilizzo si rimanda alla sezione Esecuzione runs
* __IR2018-19\_HW1.pdf :__ file pdf contenente un breve report descrittivo dell'esperienza
* __topics.351-400_trec7.txt:__ file dei topic utilizzato nel reperimento
* __qrels.trec7.txt :__ file dei qrels utilizzato nella valutazione

#### DIRECTORYs
* __runs_files :__ contiene i files generati dalle varie prove di esecuzione.  
Nella subdirectory __evaluations__ sono presenti i files relativi alla valutazione delle run effettuata con trec\_eval,  
nella subdirectory __results__ i files generati dalle varie run
nella subdirectory __plots__ i grafici relativi ai test di Tukey
* __src :__ contiene file sorgenti python di utility

Per i dati contenuti nella cartella runs\_files relativi alle prove, la distinzione per tipo di query e presenza o meno del parametro
ignore.low.idf.terms è identificata dal formato __QUERY\_\[TIPOLOGIA\]\_IGNORE\_\[PRESENZA\]__ dove:

  * TIPOLOGIA assume __TITLE__ quando il reperimento e' avvenuto utilizzando solo il campo TITLE del topic, __\_DESC__ processando anche il campo DESC del topic.   
  * PRESENZA assume il valore __FALSE__ se il parametro ignore.low.idf.terms è disattivato, __TRUE__ altrimenti.

Sia i files di valutazione che i risultati delle run sono inoltre identificati dal nome del modello utilizzato (BM25, TF\_IDF) 
seguiti da __S__ se la fase di rimozione delle stopword è presente, __nS__ se è assente, __P__ se e' presente la fase di stemming, __nP__ se non è stato utilizzato lo stemmer  
(i.e. __BM25\_S\_P__ identifica la run che utilizza il modello BM25, rimozione delle stopword e PorterStemmer)

## Esecuzione test
Utilizzando il jupyter notebook allegato è possibile rieseguire i test ANOVA e i test di Tukey. Seguire le istruzioni all'interno di esso relative all'esecuzione.  
E' ovviamente necessario avere installato nel proprio sistema una distribuzione del software del notebook con kernel Python3 e inoltre vengono richiesti dei moduli python aggiuntivi, 
nello specifico pandas, matplotlib, scipy, numpy, smart-open e statsmodel.  

Viene in seguito riportato il comando per l'installazione di questi tramite pip.

```pip install pandas matplotlib numpy scipy smart-open statsmodels```


## Esecuzione runs
E' possibile rigenerare i files presenti nella directory __runs\_files__ utilizzando lo script python __tester.py__ su sistemu UNIX. La versione di python deve essere >=3  
Per l'esecuzione è prima di tutto necessario verificare che le dipendenze consistenti nei moduli pandas e yaspin, siano soddisfatte.  
Viene in seguito riportato il comando per l'installazione di questi tramite pip.

```pip install pandas yaspin```

E' inoltre necessario avere scaricato una distribuzione di terrier e aver caricato nel sistema la collezione TREC7.

Una volta fatto ciò aprire il file __tester.py__ e modificare i parametri come richiesto immettendo il percorso completo della directory di terrier, della directory di output, del file dei topic e del qrels.

Configurato opportunatamente coi parametri voluti poi è possibile avviare lo script con il comando 

```python tester.py``` 

L'esecuzione potrebbe impiegare diversi minuti in quanto limitata inferiormente dalla velocità delle varie indicizzazioni che deve effettuare.  
Al termine nella directory di output si avra' una struttura identica alla cartella __runs\_tests__ riportata.

