import re
import pandas as pd
from smart_open import smart_open

rx_dict = {
  'MAP' : re.compile(r'^map\s+(?P<topic>\d+)\s+(?P<value>.*)\n$'),
  'Rprec': re.compile(r'^Rprec\s+(?P<topic>\d+)\s+(?P<value>.*)\n$'),
  'P10' : re.compile(r'^P_10\s+(?P<topic>\d+)\s+(?P<value>.*)\n$')
}

def _parse_line(line):
  for key, rx in rx_dict.items():
    match = rx.search(line)
    if match:
      return key, match
  return None, None

def parse_file(filepath):
  data = []
  with smart_open(filepath, 'r') as file_object:
    record = {}
    for line in file_object:
      key, match = _parse_line(line) 
      if match is None:
        continue
      topic = int(match.group('topic'))
      if bool(record.get('topic')) and record.get('topic') != topic:
        data.append(record.copy()) 
      record['topic'] = topic
      value = float(match.group('value'))
      if key == 'MAP':
        record['MAP'] = value
      elif key == 'Rprec':
        record['Rprec'] = value
      elif key == 'P10':
        record['P10'] = value
    if bool(record): data.append(record.copy())
    data = pd.DataFrame(data, columns=['topic', 'MAP', 'Rprec', 'P10'])
    data.set_index('topic')
    return data