# Autore: Giacomo Sartori
# Semplice api di interfaccia con terrier e utility varie, non production ready
# Manca gestione effettiva condizione di errore
import os
import subprocess
import re
from yaspin import yaspin, Spinner

class TerrierAPI():
  def __init__(self, terrier_folder):
    self._bin_folder = os.path.join(terrier_folder, "bin")
    self._var_folder = os.path.join(terrier_folder, "var")
    self._etc_folder = os.path.join(terrier_folder, "etc")
    self._spinner = yaspin()

  def _task_start(self, text):
    self._spinner.color = None
    self._spinner.text = text
    self._spinner.start()

  def _task_stop(self, status=None, error_message=None):
    if status is None: 
      self._spinner.stop()
    elif status == 0:
      self._spinner.color = "green"
      self._spinner.ok("OK")
    else:
      self._spinner.color = "red"
      self._spinner.fail("FAIL")
      if error_message is not None : print(error_message)

  def _clean_index(self):
    index_dir = os.path.join(self._var_folder, "index")
    try:
      files = os.listdir(index_dir)
      for f in files: os.remove(os.path.join(index_dir,f))
    except FileNotFoundError: pass

  def clean_results(self):
    results_dir = os.path.join(self._var_folder, "results")
    try:
      files = os.listdir(results_dir)
      for f in files: os.remove(os.path.join(results_dir,f))
    except FileNotFoundError: pass
   
  def move_results(self, output):
    results_dir = os.path.join(self._var_folder, "results")
    destination_folder = os.path.join(output)
    if not os.path.exists(os.path.dirname(destination_folder)): os.makedirs(os.path.dirname(destination_folder))
    self._task_start("Move results files to " + destination_folder)
    status = 0
    error_message = None
    try:
      os.rename(results_dir, destination_folder)
    except (FileNotFoundError, OSError) as e:
      status = 1
      if isinstance(e, FileNotFoundError): error_message = "Cannot find " + results_dir
      elif isinstance(e, OSError): error_message = "Destination folder or file already present, please proceed wih manual copy"
      pass
    self._task_stop(status, error_message)
    return status

  def make_index(self):
    terrier_bin = os.path.join(self._bin_folder, "trec_terrier.sh")
    self._task_start("Make index")
    status = subprocess.call([terrier_bin, "-i"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    self._task_stop(status)
    return status

  def make_retrieval(self, model, topics, filename=None):
    terrier_bin = os.path.join(self._bin_folder, "trec_terrier.sh")
    callargs = [terrier_bin, "-r", "-Dtrec.model="+model, "-Dtrec.topics="+topics]
    if (filename is not None):  callargs.append("-Dtrec.results.file="+filename+".res")
    self._task_start("Make retrieval using " + model)   
    status = subprocess.call(callargs, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    self._task_stop(status)
    return status

  def evaluate(self, qrels_file, result_file, output_file):
    trec_eval = os.path.join(self._bin_folder, "trec_eval.sh")
    status = 0
    self._task_start("Evaluate " + result_file)
    with open(output_file, "w") as outfile:
      status = subprocess.call([trec_eval, qrels_file, result_file, "-q"], stdout=outfile)
    self._task_stop(status)
    return status

  def evaluate_all(self, qrels_file, destination_folder, results_folder=None):
    if results_folder is None: results_folder = os.path.join(self._var_folder, "results")
    if not os.path.exists(destination_folder): os.makedirs(destination_folder)
    try:
      files = os.listdir(results_folder)
      for f in files:
        if not f.endswith(".res"): continue
        result_file = os.path.join(results_folder, f)
        output_file = os.path.join(destination_folder, f[:-4])
        self.evaluate(qrels_file, result_file, output_file)
    except FileNotFoundError:
      print("FAIL Evaluate all\nNo directory ", results_folder)
      return 1

  @staticmethod
  def _parse_line(line, rx_dict):
    for key, rx in rx_dict.items():
      match = rx.search(line)
      if match: return key
    return None

  def configure_properties(self,properties, basefile=None):
    self._clean_index()
    status = 0
    if basefile is None: 
      basefile = os.path.join(self._etc_folder, "terrier.properties.sample")
      print("Configure system with basefile", basefile)
    file_path = os.path.join(self._etc_folder, "terrier.properties")
    prop_names = {}
    for prop in properties.keys():
      prop_names[prop] = re.compile(r'^'+prop)
    output_file = open(file_path, "w")
    self._task_start("Configure system properties")
    try:
      with open(basefile, "r") as file_input:
        for line in file_input:
          prop = self._parse_line(line, prop_names)
          if ( prop is not None ):
            line = prop+"="+properties.pop(prop, "")+"\n"
          output_file.write(line)
      for prop,value in properties.items():
        output_file.write(prop+"="+value+"\n")
    except IOError: status = 1
    output_file.close()
    self._task_stop(status)

