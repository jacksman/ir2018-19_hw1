# Autore: Giacomo Sartori
# Note: non c'è una gestione effettiva dell'errore ma solo una segnalazione di esso 
# Non è un codice pulito ma lo scopo e' unicamente di automatizzare i test
# L'esecuzione richiede molto tempo, vedi main per limitare i test
import os
from src.terrier import TerrierAPI

## MODIFICARE CON I PERCORSI OPPORTUNI
terrier_folder = "/Users/jack/Desktop/University/IR/terrier-core-4.4"
output_folder = "/Users/jack/Desktop/HW1/runs_files"
topics_file = "/Users/jack/Desktop/ir2018-19_hw1/topics.351-400_trec7.txt"
qrels_file = "/Users/jack/Desktop/ir2018-19_hw1/qrels.trec7.txt"


def perform_test(desc=False, ignorelow=False):

  terrier = TerrierAPI(terrier_folder)
  
  folder_name   = "QUERY_" + ("_DESC" if desc else "TITLE") + "_IGNORE_" + ("TRUE" if ignorelow else "FALSE")
  base_conf = {}
  base_conf["TrecQueryTags.process"] = "TOP,NUM,TITLE" + ( ",DESC" if desc else "")
  base_conf["TrecQueryTags.skip"] = ( "DESC," if not desc else "" ) + "NARR"
  base_conf["ignore.low.idf.terms"] = "true" if ignorelow else "false"
  basefile = os.path.join(os.path.dirname(os.path.abspath(__file__)),"src", "default.properties")
  """
  print("[+] PERFORM TEST " + folder_name)
  
  terrier.clean_results()
  
  print("[+] Run 1/3 (BM25 TF_IDF Stopwords,PorterStemmer)")
  terrier.configure_properties({
    **base_conf,
    "termpipelines": "Stopwords,PorterStemmer",
  }, basefile)
  terrier.make_index()
  terrier.make_retrieval("BM25", topics_file, "BM25_S_P")
  terrier.make_retrieval("TF_IDF", topics_file, "TF_IDF_S_P")
  
  print("[+] Run 2/3 (BM25 no Stopwords,PorterStemmer)")
  terrier.configure_properties({
    **base_conf,
    "termpipelines": "PorterStemmer"
  }, basefile)
  terrier.make_index()
  terrier.make_retrieval("BM25", topics_file, "BM25_nS_P")

  print("[+] Run 3/3 (TF_IDF no Stopwords, no PorterStemmer)")
  terrier.configure_properties({
    **base_conf,
    "termpipelines": ""
  }, basefile)
  terrier.make_index()
  terrier.make_retrieval("TF_IDF", topics_file, "TF_IDF_nS_nP")

  # valutazione finale e copia dei risultati
  """
  terrier.evaluate_all(qrels_file, os.path.join(output_folder, "evaluations", folder_name))
  terrier.move_results(os.path.join(output_folder, "results", folder_name))

if __name__ == '__main__':
  perform_test(False, False) # Effettua il test con parametri query TITLE e IGNORE a false
  perform_test(False, True)  # Effettua il test con parametri query TITLE e IGNORE a true
  perform_test(True, False)  # Effettua il test con parametri query TITLE,DESC e IGNORE a false
  perform_test(True, True)  # Effettua il test con parametri query TITLE,DESC e IGNORE a true

  